import React from 'react'
import './styles.scss'

function CheckBox({label, id, isChecked, onChange}) {
  return (
    <div className='checkbox'>
      <input
        type='checkbox'
        className='checkbox-input'
        id={id}
        value={id}
        checked={isChecked}
        onChange={onChange}
      />
      <label htmlFor={id}>{label}</label>
    </div>
  )
}

export default CheckBox
