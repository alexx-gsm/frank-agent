import React from 'react'
import {Redirect} from 'react-router-dom'
import useLocalStorage from '../../hooks/useLocalStorage'
import NavBar from '../../components/NavBar'
import AppBar from '../../components/AppBar'
import './styles.scss'

function AdminLayout({children}) {
  const [token] = useLocalStorage('token')

  if (!token) {
    return <Redirect to='/signIn' />
  }

  return (
    <div className='page'>
      <NavBar />
      <AppBar />
      <div className='content'>{children}</div>
    </div>
  )
}

export default AdminLayout
