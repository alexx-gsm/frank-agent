import React from 'react'
import './styles.scss'

function Footer() {
  return (
    <footer className='footer'>
      <div className='copyright'>© 1992 - 2020 Честный Агент © Все права защищены.</div>
      <div className='contacts'>8 (495) 150-21-12 </div>
    </footer>
  )
}

export default Footer
