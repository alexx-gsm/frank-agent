import React from 'react'
import {NavLink} from 'react-router-dom'

function AppMenuItem({item}) {
  return (
    <NavLink exact={item.exact} to={item.url} className='appmenu-link' activeClassName='selected'>
      {item.icon}
      {item.title}
    </NavLink>
  )
}

export default AppMenuItem
