import React, {useState, useEffect, useRef} from 'react'
import classNames from 'classnames'
import './styles.scss'

function Select({label, value, list = [], onUpdate}) {
  const fieldRef = useRef()
  const [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    function handleClickOutside(e) {
      if (fieldRef.current && !fieldRef.current.contains(e.target)) {
        setIsOpen(false)
      }
    }
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [fieldRef])

  const handleClick = () => {
    setIsOpen(!isOpen)
  }

  const handleSelect = (val) => {
    if (val !== value) {
      onUpdate(val)
    }
    setIsOpen(false)
  }

  return (
    <div className='field' ref={fieldRef}>
      <div className={classNames('select', isOpen && 'is-open')} onClick={handleClick}>
        {label && <label className='select-label'>{label}</label>}
        <div className='select-input'>{value}</div>
      </div>
      {isOpen && (
        <div className={'select-dropbox'}>
          <div className='list'>
            {list.map((item) => {
              return (
                <div
                  key={item.title}
                  className='list-item'
                  onClick={() => handleSelect(item.title)}
                >
                  {item?.title}
                </div>
              )
            })}
            {!list.length && (
              <div className='list-item is-empty' onClick={handleClick}>
                {'пусто'}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  )
}

export default Select
