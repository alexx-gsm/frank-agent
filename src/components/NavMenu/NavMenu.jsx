import React from 'react'
import NavMenuItem from '../NavMenuItem'

const renderMenuItem = (item) => <NavMenuItem key={item.id} item={item} />

const renderMenu = (items) => items.map(renderMenuItem)

function NavMenu({items = []}) {
  return <div className='navmenu'>{renderMenu(items)}</div>
}

export default NavMenu
