import React from 'react'
import CheckBox from '../CheckBox/CheckBox'
import './styles.scss'

function MultiCheckBox({label, typeValues, checkedValues, onUpdate}) {
  const handleChange = (e) => {
    const newCheckedValues = e.target.checked
      ? [...checkedValues, e.target.value]
      : checkedValues.filter((v) => v !== e.target.value)

    onUpdate?.(newCheckedValues)
  }

  return (
    <div className='field'>
      <div className='multi-checkbox'>
        {label && <label className='multi-checkbox-label'>{label}</label>}
        {Object.keys(typeValues).map((type, key) => {
          const isChecked = checkedValues.includes(type)

          return (
            <CheckBox
              key={key}
              id={type}
              label={typeValues[type]}
              isChecked={isChecked}
              onChange={handleChange}
            />
          )
        })}
      </div>
    </div>
  )
}

export default MultiCheckBox
