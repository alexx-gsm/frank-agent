import React from 'react'
import {ReactComponent as BuildingIcon} from '../../icons/Building.svg'

export const APPBAR_HEADER = {
  title: 'ЧЕСТНЫЙ АГЕНТ',
  type: 'МЕНЕДЖЕР ПРОЦЕССА',
}

export const APPBAR_DATA = {
  dashboard: {
    menu: [
      {
        title: 'Доска',
        alias: 'dashboard',
        url: '/',
        icon: <BuildingIcon width={20} height={20} className='icon' />,
      },
    ],
  },
  markets: {
    menu: [
      {
        title: 'Организации',
        alias: 'markets',
        url: '/markets',
        icon: <BuildingIcon width={20} height={20} className='icon' />,
      },
    ],
  },
  search: {
    menu: [
      {
        title: 'Поиск',
        alias: 'search',
        url: '/search',
        icon: <BuildingIcon width={20} height={20} className='icon' />,
      },
    ],
  },
  settings: {
    menu: [
      {
        title: 'Настройки',
        alias: 'settings',
        url: '/settings',
        exact: true,
        icon: <BuildingIcon width={20} height={20} className='icon' />,
      },
      {
        title: 'Профиль',
        alias: 'profile',
        url: '/settings/profile',
        icon: <BuildingIcon width={20} height={20} className='icon' />,
      },
    ],
  },
  chat: {
    menu: [
      {
        title: 'Чат',
        alias: 'chat',
        url: '/chat',
        icon: <BuildingIcon width={20} height={20} className='icon' />,
      },
    ],
  },
  logout: {
    menu: [
      {
        title: 'Выход',
        alias: 'logout',
        url: '/logout',
        icon: <BuildingIcon width={20} height={20} className='icon' />,
      },
    ],
  },
}
