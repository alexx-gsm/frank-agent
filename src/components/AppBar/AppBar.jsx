import React from 'react'
import {useLocation} from 'react-router-dom'
import AppBarHeader from '../AppBarHeader'
import AppMenuItem from '../AppMenuItem'
import {APPBAR_HEADER, APPBAR_DATA} from './data'
import {ROOT_PATH_TITLE} from '../../constants'
import {getPath} from '../../utils/commonUtils'
import './styles.scss'

const getAgentInfo = () => APPBAR_HEADER

const getMenu = (path) => APPBAR_DATA[path]?.menu ?? []

const renderMenuItem = (item) => <AppMenuItem key={item.alias} item={item} />

const renderMenu = (items) => items.map(renderMenuItem)

function AppBar() {
  const {pathname} = useLocation()
  const path = getPath(pathname, ROOT_PATH_TITLE)
  const menu = getMenu(path)

  return (
    <div className='appbar'>
      <AppBarHeader agent={getAgentInfo()} />
      {renderMenu(menu)}
    </div>
  )
}

export default AppBar
