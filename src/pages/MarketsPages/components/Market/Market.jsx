import React, {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import CommonInfoCard from '../CommonInfoCard'
import ContactCard from '../ContactCard'
import {useMarketsContext} from '../../../../stores/markets'
import {ReactComponent as EditIcon} from '../../../../icons/Edit.svg'
import PhotosSection from '../PhotosSection/PhotosSection'
import Field from '../../../../components/Field/Field'

function Market() {
  const {slug} = useParams()
  const {state, getMarket, getContact, editMarket, saveMarket} = useMarketsContext()
  const [isEdited, setIsEdited] = useState(false)

  useEffect(() => {
    if (!slug) {
      return
    }
    getMarket(slug)
  }, [slug])

  useEffect(() => {
    const contactId = state.market?.contactId ?? null
    const id = state.contact?.id
    if (!contactId || contactId === id) {
      return
    }
    getContact(contactId)
  }, [state.market])

  const handleSubmitMarket = (fields) => () => {
    setIsEdited(false)
    saveMarket(fields)
  }

  if (!state.market) {
    return ''
  }

  return (
    <div className='main'>
      <div className='main-title'>
        <Field
          value={state.market.shortName}
          isEdited={isEdited}
          onChange={editMarket('shortName')}
          onSubmit={handleSubmitMarket('shortName')}
          isAutoFocus={true}
        />
        {!isEdited && (
          <EditIcon
            width={20}
            height={20}
            className='main-title-icon'
            onClick={() => setIsEdited(true)}
          />
        )}
      </div>
      <CommonInfoCard />
      <ContactCard />
      <PhotosSection />
    </div>
  )
}

export default Market
