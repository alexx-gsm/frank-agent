import React, {useState} from 'react'
import {ReactComponent as EditIcon} from '../../icons/Edit.svg'
import {ReactComponent as LinkedIcon} from '../../icons/Linked.svg'
import Field from '../Field'
import MultiFields from '../MultiFields'
import DayPicker from '../DayPicker'
import Select from '../Select'
import MultiCheckBox from '../MultiCheckBox'
import PhoneInput from '../PhoneInput'
import {FIELD_TYPES} from '../../constants'
import './styles.scss'

const renderItem = (item, key) => {
  return (
    <div key={key} className='card-item'>
      <div className='card-label'>{item?.label}:</div>
      <div className='card-value'>
        {item.isEmail ? (
          <a href={`mailto:${item?.value}`} className='email'>
            {item?.value}
          </a>
        ) : (
          item?.value
        )}
      </div>
    </div>
  )
}

const renderItems = (items) => items?.map(renderItem)

function Card({card = {}, onChange, onSubmit, onUpdate}) {
  const [isEdit, setIsEdit] = useState(false)

  return (
    <div className='card'>
      <div className='card-title'>
        {card.title}
        {isEdit ? (
          <LinkedIcon
            width={20}
            height={20}
            className='card-icon'
            onClick={() => setIsEdit(false)}
          />
        ) : (
          <EditIcon width={20} height={20} className='card-icon' onClick={() => setIsEdit(true)} />
        )}
      </div>
      {isEdit
        ? card.fields.map((field, key) => {
            switch (field.fieldType) {
              case FIELD_TYPES.TEXT:
                return (
                  <Field
                    key={key}
                    label={field.label}
                    value={field.value}
                    isEdited={true}
                    onChange={onChange(field.fieldName)}
                    onSubmit={onSubmit(field.fieldName)}
                  />
                )
              case FIELD_TYPES.MULTI_TEXT:
                return (
                  <MultiFields
                    key={key}
                    list={field.list}
                    onChange={onChange}
                    onSubmit={onSubmit}
                  />
                )
              case FIELD_TYPES.DAYPICKER:
                return (
                  <DayPicker
                    key={key}
                    fieldNames={field.fieldNames}
                    data={field.data}
                    onChange={onChange}
                    onUpdate={onUpdate(field.fieldName)}
                  />
                )
              case FIELD_TYPES.SELECT:
                return (
                  <Select
                    key={key}
                    label={field.label}
                    value={field.value}
                    list={field.list}
                    onUpdate={onUpdate(field.fieldName)}
                  />
                )
              case FIELD_TYPES.CHECKBOX:
                return (
                  <MultiCheckBox
                    key={key}
                    label={field.label}
                    checkedValues={field.checkedValues}
                    typeValues={field.typeValues}
                    onUpdate={onUpdate(field.fieldName)}
                  />
                )
              case FIELD_TYPES.PHONE:
                return (
                  <PhoneInput
                    key={key}
                    label={field.label}
                    value={field.value}
                    onChange={onChange(field.fieldName)}
                    onSubmit={onSubmit(field.fieldName)}
                  />
                )
              default:
                return ''
            }
          })
        : renderItems(card.fields)}
    </div>
  )
}

export default Card
