import React from 'react'
import classNames from 'classnames'
import './styles.scss'

function Button({title, onClick, color, isDisabled}) {
  const handleClick = (e) => {
    e.preventDefault()
    onClick?.()
  }

  const className = classNames('button', color && `button-${color}`, isDisabled && 'disabled')

  return (
    <button onClick={handleClick} className={className} disabled={isDisabled}>
      {title}
    </button>
  )
}

export default Button
