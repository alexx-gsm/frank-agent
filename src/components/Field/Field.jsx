import React from 'react'
import classNames from 'classnames'
import './styles.scss'
import {KEYCODE_ENTER} from '../../constants'

const renderField = (value) => <div className='field'>{value}</div>

const renderInput = ({
  label,
  value,
  isAutoFocus,
  classes,
  handleChange,
  handleKeyup,
  handleOnBlur,
}) => {
  return (
    <div className={classNames('field', classes)}>
      {label && <label className='field-label'>{label}</label>}
      <input
        type='text'
        className='field-input'
        value={value}
        autoFocus={isAutoFocus}
        onChange={handleChange}
        onKeyUp={handleKeyup}
        onBlur={handleOnBlur}
      />
    </div>
  )
}

function Field({label, value, isEdited, isAutoFocus, classes, onChange, onSubmit}) {
  const handleChange = (e) => {
    onChange(e.target.value)
  }

  const handleKeyup = (e) => {
    if (e.keyCode === KEYCODE_ENTER) {
      onSubmit?.()
    }
  }

  const handleOnBlur = () => onSubmit?.()

  return isEdited
    ? renderInput({
        label,
        value,
        isAutoFocus,
        classes,
        handleChange,
        handleKeyup,
        handleOnBlur,
      })
    : renderField(value)
}
export default Field
