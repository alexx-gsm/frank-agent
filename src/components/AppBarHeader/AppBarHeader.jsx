import React from 'react'
import './styles.scss'

function AppBarHeader({agent}) {
  return (
    <div className='appbar-header'>
      <div className='title'>{agent?.title}</div>
      <div className='subtitle'>{agent?.type}</div>
    </div>
  )
}

export default AppBarHeader
