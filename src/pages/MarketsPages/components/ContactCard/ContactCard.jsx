import React from 'react'
import {useMarketsContext} from '../../../../stores/markets'
import Card from '../../../../components/Card/Card'
import {FIELD_TYPES} from '../../../../constants'

const formatPhoneNumber = (phone) => {
  const cleaned = ('' + phone).replace(/\D/g, '')
  const match = cleaned.match(/^(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})$/)
  if (match) {
    let intlCode = match[1] ? `+${match[1]} ` : ''
    return [intlCode, '(', match[2], ') ', match[3], '-', match[4], '-', match[5]].join('')
  }

  return null
}

const mapContact = (contact) => {
  const {lastname, firstname, patronymic, phone, email} = contact

  return {
    title: 'КОНТАКТНЫЕ ДАННЫЕ',
    fields: [
      {
        label: 'ФИО',
        value: `${lastname} ${firstname} ${patronymic}`,
        fieldName: '',
        list: [
          {label: 'Фамилия', fieldName: 'lastname', value: lastname},
          {label: 'Имя', fieldName: 'firstname', value: firstname},
          {label: 'Отчество', fieldName: 'patronymic', value: patronymic},
        ],
        fieldType: FIELD_TYPES.MULTI_TEXT,
      },
      {
        label: 'Телефон',
        value: formatPhoneNumber(phone),
        fieldName: 'phone',
        fieldType: FIELD_TYPES.PHONE,
      },
      {
        label: 'Эл. почта',
        value: email,
        isEmail: true,
        fieldName: 'email',
        fieldType: FIELD_TYPES.TEXT,
      },
    ],
  }
}

function ContactCard() {
  const {state, editContact, saveContact} = useMarketsContext()

  const handleSubmitContact = (fields) => () => {
    saveContact(fields)
  }

  if (!state.contact) {
    return ''
  }
  return (
    <Card card={mapContact(state.contact)} onChange={editContact} onSubmit={handleSubmitContact} />
  )
}

export default ContactCard
