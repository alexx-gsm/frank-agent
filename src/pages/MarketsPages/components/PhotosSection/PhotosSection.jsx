import React, {useState} from 'react'
import moment from 'moment'
import 'moment/locale/ru'
import IconButton from '../../../../components/IconButton'
import UploadButton from '../../../../components/UploadButton'
import Button from '../../../../components/Button'
import Modal from '../../../../components/Modal'
import {ReactComponent as IconAdd} from '../../../../icons/Add.svg'
import {ReactComponent as IconClose} from '../../../../icons/Close.svg'
import {useMarketsContext} from '../../../../stores/markets'
import './styles.scss'

const TITLE = 'ПРИЛОЖЕННЫЕ ФОТО'
const BUTTON_TITLE = 'ДОБАВИТЬ ИЗОБРАЖЕНИЕ'
const MODAL_ADD_PHOTO = {
  TITLE: 'Загрузить изображение',
  CONTENT: 'Выберите изображение и нажмите "Загрузить"',
  CANCEL_BUTTON: 'Отмена',
  ACTION_BUTTON: 'Загрузить',
}
const PHOTO_TYPES = ['image/png', 'image/jpeg', 'image/gif']
const MOCK_IMAGE_DATE = moment().format('D MMMM YYYY')

function PhotosSection({photos = []}) {
  const [isOpenModal, setIsOpenModal] = useState(false)
  const [file, setFile] = useState(null)

  const {state, uploadFile, deleteFile} = useMarketsContext()

  const handleOpen = () => {
    setFile(null)
    setIsOpenModal(true)
  }

  const handleClose = () => setIsOpenModal(false)

  const handleChange = (e) => {
    const file = Array.from(e.target.files)?.[0] ?? null

    if (PHOTO_TYPES.includes(file.type)) {
      setFile(file)
    }
  }

  const handleSubmit = async () => {
    if (!file) {
      return
    }

    const formData = new FormData()
    formData.append('file', file)

    await uploadFile(formData)

    setIsOpenModal(false)
  }

  const handleDelete = (name) => async () => {
    await deleteFile(name)
  }

  const renderPhoto = (photo, key) => {
    const {name, thumbpath} = photo
    return (
      <div key={key} className='photo'>
        <img src={thumbpath} className='photo-image' alt='' />
        <div className='photo-title'>{name}</div>
        <div className='photo-date'>{MOCK_IMAGE_DATE}</div>
        <IconClose
          width={20}
          height={20}
          className='icon icon-close'
          onClick={handleDelete(photo.name)}
        />
      </div>
    )
  }

  const renderPhotos = (photos) => photos.map(renderPhoto)

  const renderModalContent = () => {
    return (
      <>
        <div className='message'>{MODAL_ADD_PHOTO.CONTENT}</div>
        <UploadButton label='Выбрать' onChange={handleChange} />
        <div className='file-name'>{file?.name}</div>
      </>
    )
  }

  const renderModalActions = () => {
    return (
      <>
        <Button title={MODAL_ADD_PHOTO.CANCEL_BUTTON} onClick={() => handleClose()} />
        <Button
          title={MODAL_ADD_PHOTO.ACTION_BUTTON}
          onClick={() => handleSubmit()}
          color='primary'
          isDisabled={!file}
        />
      </>
    )
  }

  return (
    <>
      <div className='section'>
        <div className='section-title'>{TITLE}</div>
        <div className='section-photos'>{renderPhotos(state.market.photos)}</div>
        <IconButton title={BUTTON_TITLE} Icon={IconAdd} onClick={handleOpen} />
      </div>
      <Modal
        isOpen={isOpenModal}
        header={MODAL_ADD_PHOTO.TITLE}
        content={renderModalContent()}
        actions={renderModalActions()}
        file={file}
      />
    </>
  )
}

export default PhotosSection
