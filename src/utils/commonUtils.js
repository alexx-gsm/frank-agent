export const getPath = (pathname, value = '') => {
  const path = pathname?.split('/', 2)[1]
  return path === '' ? value : path
}
