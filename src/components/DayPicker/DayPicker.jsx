import React, {useState} from 'react'
import DatePicker from 'react-datepicker'
import ru from 'date-fns/locale/ru'
import moment from 'moment'
import Field from '../Field'
import './styles.scss'
import 'react-datepicker/dist/react-datepicker.css'

const formatDate = (date) =>
  moment(date)
    .toISOString()
    .replace(/[.]\d+/, '')

function DayPicker({fieldNames, data, onUpdate}) {
  const {text, date} = fieldNames
  const [no, setNo] = useState(data[text.field])
  const [issue_date, setDay] = useState(new Date(data[date.field]))

  const handleNoSubmit = () => {
    onUpdate({no, issue_date: formatDate(date)})
  }

  const handleDateSubmit = (date) => {
    setDay(date)
    onUpdate({no, issue_date: formatDate(date)})
  }

  return (
    <div className='daypicker'>
      <Field
        label={text.label}
        value={no}
        onChange={setNo}
        onSubmit={handleNoSubmit}
        isEdited={true}
        classes='small'
      />
      <div className='daypicker-field field small'>
        <label className='field-label'>{date.label}</label>
        <DatePicker
          selected={issue_date}
          onChange={handleDateSubmit}
          locale={ru}
          dateFormat='dd.MM.yyyy'
          popperPlacement='top-end'
          popperClassName='picker'
          todayButton='Сегодня'
        />
      </div>
    </div>
  )
}

export default DayPicker
