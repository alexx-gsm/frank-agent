import React from 'react'
import moment from 'moment'
import {useMarketsContext} from '../../../../stores/markets'
import Card from '../../../../components/Card/Card'
import {AGENT_TYPES, FIELD_TYPES, BUSINESS_ENTITIES} from '../../../../constants'

const formatDate = (date) => moment(date).format('D.MM.YYYY')

const getTypes = (type) => type.map((t) => AGENT_TYPES[t]).join(', ')

const mapMarket = (market) => {
  const {name, contract, businessEntity, type} = market

  return {
    title: 'ОБЩАЯ ИНФОРМАЦИЯ',
    fields: [
      {
        label: 'Полное название',
        value: name,
        fieldName: 'name',
        fieldType: FIELD_TYPES.TEXT,
      },
      {
        label: 'Договор',
        value: `${contract?.no} от ${formatDate(contract.issue_date)}`,
        fieldName: 'contract',
        data: contract,
        fieldNames: {
          text: {field: 'no', label: 'Номер договора'},
          date: {field: 'issue_date', label: 'Дата договора'},
        },
        fieldType: FIELD_TYPES.DAYPICKER,
      },
      {
        label: 'Форма',
        value: businessEntity,
        fieldName: 'businessEntity',
        list: BUSINESS_ENTITIES,
        fieldType: FIELD_TYPES.SELECT,
      },
      {
        label: 'Тип',
        value: getTypes(type),
        fieldName: 'type',
        checkedValues: type,
        typeValues: AGENT_TYPES,
        fieldType: FIELD_TYPES.CHECKBOX,
      },
    ],
  }
}

function CommonInfoCard() {
  const {state, editMarket, saveMarket, updateMarket} = useMarketsContext()

  const handleSubmitMarket = (fields) => () => {
    saveMarket(fields)
  }

  if (!state.market) {
    return ''
  }

  return (
    <Card
      card={mapMarket(state.market)}
      onChange={editMarket}
      onSubmit={handleSubmitMarket}
      onUpdate={updateMarket}
    />
  )
}

export default CommonInfoCard
