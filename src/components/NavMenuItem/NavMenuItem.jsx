import React from 'react'
import {NavLink} from 'react-router-dom'
import './styles.scss'

export default function NavMenuItem({item}) {
  const {title, url, icon} = item || {}

  return (
    <NavLink to={url} exact={url === '/'} className='navmenu-link' activeClassName='selected'>
      <img src={icon} alt={title} className='link-icon' />
    </NavLink>
  )
}
