export const TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiVVNFUk5BTUUiLCJpYXQiOjE2MTE1NjYzOTgsImV4cCI6MTYxMjE3MTE5OH0.M_eb2t6XbRwad8WTWPjtALoFheTe2FWt5cyhPMKO4eo'

export const APP_NAME = 'FRANK-AGENT'

export const APP_LOCAL_TOKEN = 'FrankAgentLocalAppState'

export const HOST = 'http://135.181.35.61:2112'

export const USERNAME_MIN_LENGTH = 3
