import React, {useEffect} from 'react'
import MarketListItem from '../MarketsListItem'
import {useMarketsContext} from '../../../../stores/markets'
import './styles.scss'

const renderListItem = (item) => <MarketListItem key={item.id} item={item} />

const renderLists = (items) => items.map(renderListItem)

function MarketsList() {
  const {state, getAll} = useMarketsContext()

  useEffect(() => {
    getAll()
  }, [getAll])

  return <div className='main'>{renderLists(state.markets)}</div>
}

export default MarketsList
