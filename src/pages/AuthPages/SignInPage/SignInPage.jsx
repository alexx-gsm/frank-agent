import React, {useState, useEffect} from 'react'
import {Redirect} from 'react-router-dom'
import Button from '../../../components/Button'
import Field from '../../../components/Field'
import {useAuthContext} from '../../../stores/auth'
import useLocalStorage from '../../../hooks/useLocalStorage'
import './styles.scss'

function SignInPage() {
  const {state, setValue, isValid, signIn} = useAuthContext()
  const [token, setToken] = useLocalStorage('token')
  const [isLogged, setIsLogged] = useState(false)

  useEffect(() => {
    if (token && state.isLoggedIn && state.token) {
      setIsLogged(true)
    }
  }, [token, state.token, state.isLoggedIn, setIsLogged])

  const handleChange = (e) => {
    setValue(e)
  }

  const handleSubmit = async () => {
    setToken('')
    const token = await signIn()
    setToken(token)
    setIsLogged(true)
  }

  return isLogged ? (
    <Redirect to='/' />
  ) : (
    <div className='auth-page'>
      <div className='auth-page-wrapper'>
        <h1>Честный Агент</h1>
        <Field value={state?.username} onChange={handleChange} isEdited={true} />
        <div className='action'>
          <Button
            title='Войти'
            color='primary'
            onClick={() => handleSubmit()}
            isDisabled={!isValid()}
          />
        </div>
      </div>
    </div>
  )
}

export default SignInPage
