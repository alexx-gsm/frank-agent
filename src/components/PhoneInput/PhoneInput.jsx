import React, {useState, useEffect} from 'react'
import InputMask from 'react-input-mask'
import classNames from 'classnames'
import './styles.scss'

function PhoneInput({label, value, onChange, onSubmit}) {
  const [isValid, setIsValid] = useState(false)
  const [phone, setPhone] = useState(value)

  useEffect(() => {
    const phoneNumbers = ('' + phone).replace(/\D/g, '')
    if (phoneNumbers.length < 11) {
      setIsValid(false)
      return
    }

    setIsValid(true)
    onChange(phoneNumbers)
  }, [phone])

  const handleOnBlur = () => {
    if (isValid) {
      onSubmit()
    }
  }

  return (
    <div className='field'>
      <div className='phone'>
        {label && <label className='field-label'>{label}</label>}
        <InputMask
          mask='+7 (999) 999-99-99'
          alwaysShowMask={true}
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
          onBlur={handleOnBlur}
          className={classNames('phone-input', !isValid && 'invalid')}
        />
      </div>
    </div>
  )
}

export default PhoneInput
