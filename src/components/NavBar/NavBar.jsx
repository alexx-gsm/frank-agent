import React from 'react'
import NavMenu from '../NavMenu'
import {TOP_MENU_ITEMS, BOTTOM_MENU_ITEMS} from './menu'
import './styles.scss'

function NavBar() {
  return (
    <nav className='navbar'>
      <NavMenu items={TOP_MENU_ITEMS} />
      <NavMenu items={BOTTOM_MENU_ITEMS} />
    </nav>
  )
}

export default NavBar
