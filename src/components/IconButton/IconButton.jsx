import React from 'react'
import './styles.scss'

function IconButton({title, Icon, onClick}) {
  const handleClick = (e) => {
    e.preventDefault()
    onClick?.()
  }

  return (
    <button onClick={handleClick} className='icon-button'>
      {Icon && <Icon width={16} height={16} alt='' />}
      {title}
    </button>
  )
}

export default IconButton
