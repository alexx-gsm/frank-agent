import React, {useEffect} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import AdminLayout from './layouts/AdminLayout'
import useLocalStorage from './hooks/useLocalStorage'
import {useAuthContext} from './stores/auth'

import SignInPage from './pages/AuthPages/SignInPage'
import Dashboard from './pages/Dashboard'
import MarketsListPage from './pages/MarketsPages/MarketsListPage'
import MarketPage from './pages/MarketsPages/MarketPage'

function App() {
  const [token] = useLocalStorage('token')
  const {login} = useAuthContext()

  useEffect(() => {
    if (token) {
      login(token)
    }
  }, [token, login])

  return (
    <Router>
      <Switch>
        <Route path='/signin' exact component={SignInPage} />
        <AdminLayout>
          <Switch>
            <Route path='/' exact component={Dashboard} />
            <Route path='/markets' exact component={MarketsListPage} />
            <Route path='/markets/:slug' component={MarketPage} />
          </Switch>
        </AdminLayout>
      </Switch>
    </Router>
  )
}

export default App
