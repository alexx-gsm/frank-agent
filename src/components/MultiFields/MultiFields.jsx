import React from 'react'
import Field from '../Field'

function MultiFields({list = [], isAutoFocus, classes, onChange, onSubmit}) {
  return list.map((item, key) => (
    <Field
      key={key}
      label={item.label}
      value={item.value}
      isEdited={true}
      isAutoFocus={isAutoFocus}
      classes={classes}
      onChange={onChange(item.fieldName)}
      onSubmit={onSubmit(item.fieldName)}
    />
  ))
}
export default MultiFields
