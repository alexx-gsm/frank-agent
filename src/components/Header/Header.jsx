import React, {useState} from 'react'
import {useHistory} from 'react-router-dom'
import Modal from '../Modal'
import Button from '../Button'
import {useMarketsContext} from '../../stores/markets'
import {ReactComponent as LinkedIcon} from '../../icons/Linked.svg'
import {ReactComponent as RotationIcon} from '../../icons/Rotation.svg'
import {ReactComponent as TrashIcon} from '../../icons/Trash.svg'
import './styles.scss'

const MODAL_DELETE_MARKET = {
  TITLE: 'Удалить карточку',
  CONTENT: 'Отправить карточку организации в архив?',
  CANCEL_BUTTON: 'Отмена',
  ACTION_BUTTON: 'УДАЛИТЬ',
}

function Header({content = '', withIcons}) {
  const [isOpenModal, setIsOpenModal] = useState(false)
  const {deleteMarket, reloadMarket} = useMarketsContext()
  const history = useHistory()

  const handleOpen = () => setIsOpenModal(true)

  const handleClose = () => setIsOpenModal(false)

  const handleDelete = async () => {
    handleClose()
    await deleteMarket()
    history.push('/markets')
  }

  const getIcons = () => {
    return (
      <div className='icons-wrap'>
        <div className='icon icon-linked'>
          <LinkedIcon />
        </div>
        <div className='icon icon-reload'>
          <RotationIcon onClick={reloadMarket} />
        </div>
        <div className='icon icon-delete'>
          <TrashIcon onClick={handleOpen} />
        </div>
      </div>
    )
  }

  const renderModalContent = () => {
    return <div className='message'>{MODAL_DELETE_MARKET.CONTENT}</div>
  }
  const renderModalActions = () => {
    return (
      <>
        <Button title={MODAL_DELETE_MARKET.CANCEL_BUTTON} onClick={() => handleClose()} />
        <Button
          title={MODAL_DELETE_MARKET.ACTION_BUTTON}
          onClick={() => handleDelete()}
          color='primary'
        />
      </>
    )
  }

  return (
    <div className='header'>
      <h1>{content}</h1>
      {withIcons && getIcons()}

      {isOpenModal && (
        <Modal
          isOpen={isOpenModal}
          header={MODAL_DELETE_MARKET.TITLE}
          content={renderModalContent()}
          actions={renderModalActions()}
        />
      )}
    </div>
  )
}

export default Header
