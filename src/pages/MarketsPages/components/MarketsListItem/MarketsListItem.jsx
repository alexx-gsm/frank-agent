import React from 'react'
import {Link} from 'react-router-dom'
import './styles.scss'

function MarketsListItem({item}) {
  return (
    <Link to={`/markets/${item.id}`} className='market-link'>
      {item.name}
    </Link>
  )
}

export default MarketsListItem
