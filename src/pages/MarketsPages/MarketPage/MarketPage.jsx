import React from 'react'
import {Link} from 'react-router-dom'
import Header from '../../../components/Header'
import Footer from '../../../components/Footer'
import Market from '../components/Market'
import {ReactComponent as ArrowIcon} from '../../../icons/LongArrow.svg'
import {MarketsProvider} from '../../../stores/markets'

const MARKETS_PAGE_TITLE = 'К СПИСКУ ЮРИДИЧЕСКИХ ЛИЦ'

const getContent = () => (
  <>
    <Link to='/markets'>
      <ArrowIcon width={20} height={20} className='icon' />
      {MARKETS_PAGE_TITLE}
    </Link>
  </>
)

function MarketPage() {
  return (
    <main>
      <MarketsProvider>
        <Header content={getContent()} withIcons={true} />
        <Market />
        <Footer />
      </MarketsProvider>
    </main>
  )
}

export default MarketPage
