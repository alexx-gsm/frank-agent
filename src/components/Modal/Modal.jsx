import React from 'react'
import './styles.scss'

function Modal({isOpen, header, content, actions}) {
  if (!isOpen) {
    return null
  }

  return (
    <div className='modal'>
      <div className='modal-content'>
        <div className='modal-header'>{header}</div>
        <div className='modal-body'>{content}</div>
        <div className='modal-footer'>{actions}</div>
      </div>
    </div>
  )
}

export default Modal
