import React from 'react'
import Header from '../../../components/Header'
import Footer from '../../../components/Footer'
import MarketsList from '../components/MarketsList'
import {MarketsProvider} from '../../../stores/markets'

const MARKETS_PAGE_TITLE = 'Юридические лица'

function MarketsListPage() {
  return (
    <main>
      <MarketsProvider>
        <Header content={MARKETS_PAGE_TITLE} />
        <MarketsList />
        <Footer />
      </MarketsProvider>
    </main>
  )
}

export default MarketsListPage
