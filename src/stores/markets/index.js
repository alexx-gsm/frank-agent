import React from 'react'
import {Record} from 'immutable'
import axios from 'axios'
import {APP_NAME, HOST, TOKEN} from '../../config'

import {MARKETS} from './mock'

const authHeader = {
  Authorization: `Beare ${TOKEN}`,
}

const MODULE_NAME = `${APP_NAME}/MARKETS`

const GET_ALL = `${MODULE_NAME}/GET_ALL`
const GET_MARKET = `${MODULE_NAME}/GET_MARKET`
const EDIT_MARKET = `${MODULE_NAME}/EDIT_MARKET`
const SAVE_MARKET = `${MODULE_NAME}/SAVE_MARKET`
const GET_CONTACT = `${MODULE_NAME}/GET_CONTACT`
const EDIT_CONTACT = `${MODULE_NAME}/EDIT_CONTACT`
const SAVE_CONTACT = `${MODULE_NAME}/SAVE_CONTACT`
const SET_PHOTOS = `${MODULE_NAME}/SET_PHOTOS`
const SET_ERROR = `${MODULE_NAME}/SET_ERROR`

const LOADING_START = `${MODULE_NAME}/LOADING_START`
const LOADING_FINISH = `${MODULE_NAME}/LOADING_FINISH`

const MarketsContext = React.createContext()

const ReducerRecord = Record({
  markets: [],
  market: null,
  contact: null,
  isLoading: false,
  error: null,
})

function reducer(state, action) {
  const {type, payload} = action
  switch (type) {
    case LOADING_START: {
      return state.set('isLoading', true)
    }
    case LOADING_FINISH: {
      return state.set('isLoading', false)
    }
    case GET_ALL: {
      return state.set('markets', payload.markets).set('isLoading', false)
    }
    case GET_MARKET: {
      return state.set('market', payload.market).set('isLoading', false)
    }
    case EDIT_MARKET: {
      return state.setIn(['market', payload.field], payload.value)
    }
    case SAVE_MARKET: {
      return state.set('isLoading', false)
    }
    case GET_CONTACT: {
      return state.set('contact', payload.contact).set('isLoading', false)
    }
    case EDIT_CONTACT: {
      return state.setIn(['contact', payload.field], payload.value)
    }
    case SAVE_CONTACT: {
      return state.set('isLoading', false)
    }
    case SET_PHOTOS: {
      return state.setIn(['market', 'photos'], payload.photos).set('isLoading', false)
    }
    case SET_ERROR: {
      return state.set('error', payload.error).set('isLoading', false)
    }
    default:
      return state
  }
}

function MarketsProvider(props) {
  const [state, dispatch] = React.useReducer(reducer, new ReducerRecord())
  const value = React.useMemo(() => [state, dispatch], [state])
  return <MarketsContext.Provider value={value} {...props} />
}

function useMarketsContext() {
  const context = React.useContext(MarketsContext)
  const [state, dispatch] = context

  const getAll = () => {
    let result

    // TODO: add GET all markets ROUTE (not present in API description)
    // result = await axios.get(`${HOST}/companies`)
    dispatch({
      type: GET_ALL,
      payload: {markets: result ? result.data : MARKETS},
    })
  }

  const getMarket = async (id) => {
    dispatch({type: LOADING_START})

    try {
      const result = await axios.get(`${HOST}/companies/${id}`, {headers: authHeader})
      dispatch({
        type: GET_MARKET,
        payload: {market: result.data},
      })
      return result.data
    } catch (error) {
      dispatch({
        type: SET_ERROR,
        payload: {error},
      })
    }
  }

  const editMarket = (field) => (value) => {
    dispatch({
      type: EDIT_MARKET,
      payload: {field, value: value?.target?.value ?? value},
    })
  }

  const updateMarket = (field) => async (data) => {
    const id = state.market?.id ?? null

    if (!id) {
      return
    }

    dispatch({type: LOADING_START})

    try {
      const result = await axios.patch(
        `${HOST}/companies/${id}`,
        {[field]: data},
        {
          headers: authHeader,
        }
      )

      dispatch({
        type: GET_MARKET,
        payload: {market: result.data},
      })
    } catch (error) {
      dispatch({
        type: SET_ERROR,
        payload: {error},
      })
    }
  }

  const saveMarket = async (field) => {
    const id = state.market?.id ?? null

    dispatch({type: LOADING_START})

    try {
      if (id) {
        const result = await axios.patch(
          `${HOST}/companies/${id}`,
          {[field]: state.market[field]},
          {
            headers: authHeader,
          }
        )

        dispatch({
          type: GET_MARKET,
          payload: {market: result.data},
        })
      } else {
        // TODO: add SAVE NEW MARKET (not present in API description)
        // await axios.post(`${HOST}/companies`)

        dispatch({type: LOADING_FINISH})
      }
    } catch (error) {
      dispatch({
        type: SET_ERROR,
        payload: {error},
      })
    }
  }

  const getContact = async (id) => {
    dispatch({type: LOADING_START})

    try {
      const result = await axios.get(`${HOST}/contacts/${id}`, {headers: authHeader})
      dispatch({
        type: GET_CONTACT,
        payload: {contact: result.data},
      })
      return result.data
    } catch (error) {
      dispatch({
        type: SET_ERROR,
        payload: {error},
      })
    }
  }

  const editContact = (field) => (value) => {
    dispatch({
      type: EDIT_CONTACT,
      payload: {field, value: value?.target?.value ?? value},
    })
  }

  const saveContact = async (field) => {
    dispatch({type: LOADING_START})

    const id = state.contact?.id ?? null

    try {
      if (id) {
        const result = await axios.patch(
          `${HOST}/contacts/${id}`,
          {[field]: state.contact[field]},
          {
            headers: authHeader,
          }
        )

        dispatch({
          type: GET_CONTACT,
          payload: {contact: result.data},
        })
      } else {
        // TODO: add SAVE NEW CONTACT (not present in API description)
        // await axios.post(`${HOST}/companies`)

        dispatch({type: LOADING_FINISH})
      }
    } catch (error) {
      dispatch({
        type: SET_ERROR,
        payload: {error},
      })
    }
  }

  const uploadFile = async (formData) => {
    dispatch({type: LOADING_START})

    try {
      const result = await axios.post(`${HOST}/companies/${state.market.id}/image`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          ...authHeader,
        },
      })

      const photos = [...state.market.photos, result.data]

      dispatch({
        type: SET_PHOTOS,
        payload: {photos},
      })
    } catch (error) {
      dispatch({
        type: SET_ERROR,
        payload: {error},
      })
    }
  }

  const deleteFile = async (name) => {
    dispatch({type: LOADING_START})

    try {
      await axios.delete(`${HOST}/companies/${state.market.id}/image/${name}`, {
        headers: authHeader,
      })

      const photos = state.market.photos.filter((photo) => photo.name !== name)

      dispatch({
        type: SET_PHOTOS,
        payload: {photos},
      })
    } catch (error) {
      dispatch({
        type: SET_ERROR,
        payload: {error},
      })
    }
  }

  const deleteMarket = async () => {
    const id = state.market?.id ?? null

    if (!id) {
      return
    }

    try {
      await axios.delete(`${HOST}/companies/${id}`, {headers: authHeader})

      dispatch({
        type: GET_MARKET,
        payload: {market: null},
      })
    } catch (error) {
      dispatch({
        type: SET_ERROR,
        payload: {error},
      })
    }
  }

  const reloadMarket = async () => {
    const id = state.market?.id ?? null

    const market = await getMarket(id)

    await getContact(market?.contactId)
  }

  return {
    state,
    getAll,
    getMarket,
    editMarket,
    saveMarket,
    getContact,
    editContact,
    saveContact,
    uploadFile,
    deleteFile,
    updateMarket,
    reloadMarket,
    deleteMarket,
  }
}

export {MarketsProvider, useMarketsContext}
