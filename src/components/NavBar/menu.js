import homeIcon from '../../icons/Home.svg'
import marketIcon from '../../icons/Market.svg'
import searchIcon from '../../icons/Search.svg'
import settingsIcon from '../../icons/Settings.svg'
import chatIcon from '../../icons/Chat.svg'
import exitIcon from '../../icons/Exit.svg'

export const TOP_MENU_ITEMS = [
  {
    id: 1,
    title: 'Dashboard',
    url: '/',
    icon: homeIcon,
  },
  {
    id: 2,
    title: 'Market',
    url: '/markets',
    icon: marketIcon,
  },
  {
    id: 3,
    title: 'Search',
    url: '/search',
    icon: searchIcon,
  },
]

export const BOTTOM_MENU_ITEMS = [
  {
    id: 1,
    title: 'Settings',
    url: '/settings',
    icon: settingsIcon,
  },
  {
    id: 2,
    title: 'Chat',
    url: '/chat',
    icon: chatIcon,
  },
  {
    id: 3,
    title: 'Logout',
    url: '/logout',
    icon: exitIcon,
  },
]
