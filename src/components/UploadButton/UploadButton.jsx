import React from 'react'
import classNames from 'classnames'
import './styles.scss'

function UploadButton({label, onChange, color}) {
  const className = classNames('upload-button', color && `button-${color}`)

  return (
    <div className={className}>
      <input type='file' className='input-file' name='file' id='upload' onChange={onChange} />
      {label && (
        <label htmlFor='upload' className='label'>
          {label}
        </label>
      )}
    </div>
  )
}

export default UploadButton
