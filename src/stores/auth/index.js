import React from 'react'
import {Record} from 'immutable'
import axios from 'axios'
import {APP_NAME, HOST, USERNAME_MIN_LENGTH} from '../../config'

const MODULE_NAME = `${APP_NAME}/AUTH`

const SIGN_IN = `${MODULE_NAME}/SIGN_IN`
const SIGN_OUT = `${MODULE_NAME}/SIGN_OUT`
const SET_VALUE = `${MODULE_NAME}/SET_VALUE`
const SET_AUTH = `${MODULE_NAME}/SET_AUTH`
const LOADING_START = `${MODULE_NAME}/LOADING_START`

const AutContext = React.createContext()

const ReducerRecord = Record({
  username: '',
  isLoggedIn: false,
  token: null,
  isLoading: false,
})

function reducer(state, action) {
  const {type, payload} = action
  switch (type) {
    case LOADING_START: {
      return state.set('isLoading', true)
    }
    case SIGN_IN: {
      return state.set('isLoggedIn', true).set('token', payload?.token).set('isLoading', false)
    }
    case SIGN_OUT: {
      return state.set('isLoggedIn', false).set('token', null)
    }
    case SET_VALUE: {
      return state.set('username', payload?.username)
    }
    case SET_AUTH: {
      return state.set('token', payload?.token).set('isLoggedIn', true)
    }
    default:
      return state
  }
}

function AuthProvider(props) {
  const [state, dispatch] = React.useReducer(reducer, new ReducerRecord())
  const value = React.useMemo(() => [state, dispatch], [state])
  return <AutContext.Provider value={value} {...props} />
}

function useAuthContext() {
  const context = React.useContext(AutContext)
  const [state, dispatch] = context

  const setValue = (e) => {
    const username = e?.target?.value ?? e
    dispatch({
      type: SET_VALUE,
      payload: {username},
    })
  }

  const isValid = () => {
    return state.username.length >= USERNAME_MIN_LENGTH
  }

  const signIn = async () => {
    dispatch({type: LOADING_START})

    await axios.get(`${HOST}/auth?user=${state.username}`)
    // TODO: use API request result
    const token = 'Bearer ' + Math.random()

    dispatch({
      type: SIGN_IN,
      payload: {token},
    })

    return token
  }

  const login = (token) => {
    dispatch({
      type: SET_AUTH,
      payload: {token},
    })
  }

  return {
    state,
    setValue,
    isValid,
    signIn,
    login,
  }
}

export {AuthProvider, useAuthContext}
