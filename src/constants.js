export const ROOT_PATH_TITLE = 'dashboard'

export const KEYCODE_ENTER = 13

export const AGENT_TYPES = {
  agent: 'Агент',
  contractor: 'Подрядчик',
}

export const FIELD_TYPES = {
  TEXT: 'text',
  MULTI_TEXT: 'multi-text',
  DAYPICKER: 'daypicker',
  SELECT: 'select',
  CHECKBOX: 'checkbox',
  PHONE: 'phone',
}

export const BUSINESS_ENTITIES = [
  {id: 1, title: 'ООО'},
  {id: 2, title: 'ОАО'},
  {id: 3, title: 'ИП'},
  {id: 4, title: 'ФЛ'},
]
