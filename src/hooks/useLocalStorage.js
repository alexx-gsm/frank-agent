import {useState, useEffect} from 'react'
import {APP_LOCAL_TOKEN} from '../config'

const getLocalStorageObject = () => {
  const appLocalStorage = localStorage.getItem(APP_LOCAL_TOKEN)

  return appLocalStorage ? JSON.parse(appLocalStorage) : {}
}

const getValueByKey = (key) => {
  const objAppLocalStorage = getLocalStorageObject()

  return objAppLocalStorage ? objAppLocalStorage[key] : null
}

const setValueByKey = (key, value) => {
  const objAppLocalStorage = getLocalStorageObject()

  localStorage.setItem(APP_LOCAL_TOKEN, JSON.stringify({...objAppLocalStorage, [key]: value}))
}

function useLocalStorage(key, initialValue = '') {
  const [value, setValue] = useState(getValueByKey(key) || initialValue)

  useEffect(() => {
    setValueByKey(key, value)
  }, [key, value])

  return [value, setValue]
}

export default useLocalStorage
